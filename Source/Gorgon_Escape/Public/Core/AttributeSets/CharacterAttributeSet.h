// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"

#include "CharacterAttributeSet.generated.h"


// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnPostAttributeChanged, FGameplayAttribute, Attribute, float, OldValue, float, NewValue);

UCLASS()
class GORGON_ESCAPE_API UCharacterAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:


	/**
	 * Main Attribute
	*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health" )
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, Health)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, MaxHealth)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	FGameplayAttributeData HealthRegenerationSpeed;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, HealthRegenerationSpeed)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perify")
	FGameplayAttributeData PerifyResist;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, PerifyResist)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perify")
	FGameplayAttributeData PerifyMaxResist;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, PerifyMaxResist)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perify")
	FGameplayAttributeData PerifySpeed;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, PerifySpeed)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Strength")
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, Strength)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
	FGameplayAttributeData Speed;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, Speed)


	/**
		* Temp Attribute
	*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	FGameplayAttributeData DamageHealth;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, DamageHealth)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	FGameplayAttributeData Perify;
	ATTRIBUTE_ACCESSORS(UCharacterAttributeSet, Perify)


	/*
		
	*/

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;

	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;

	UPROPERTY(BlueprintAssignable)
	FOnPostAttributeChanged OnPostAttributeChanged;

};

