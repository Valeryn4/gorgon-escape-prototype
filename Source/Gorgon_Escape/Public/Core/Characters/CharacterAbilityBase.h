// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffect.h"

#include "CharacterAbilityBase.generated.h"



class UCharacterAbilitySystemComponent;
class UCharacterAttributeSet;
class UGameplayEffect;

UCLASS()
class GORGON_ESCAPE_API ACharacterAbilityBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterAbilityBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ability")
	UCharacterAbilitySystemComponent* BaseAbilitySystemComponent { nullptr };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ability")
	UCharacterAttributeSet* BaseAttributeSet { nullptr };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ability")
	TSubclassOf<UGameplayEffect> InitialEffect;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

};
