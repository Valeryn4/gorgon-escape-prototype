// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/AttributeSets/CharacterAttributeSet.h"
#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"
#include "GameplayEffect.h"
#include "AttributeSet.h"

void UCharacterAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{

	Super::PreAttributeChange(Attribute, NewValue);
	if (Attribute == GetHealthAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.f, GetMaxHealth());
	}
	else if (Attribute == GetPerifyResistAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.f, GetPerifyMaxResist());
	}

}

void UCharacterAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{

	OnPostAttributeChanged.Broadcast(Attribute, OldValue, NewValue);
}

void UCharacterAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	auto CurrentAttribute = Data.EvaluatedData.Attribute;
	if (CurrentAttribute == GetDamageHealthAttribute())
	{

		float CurrentDamage = GetDamageHealth();
		SetDamageHealth(0.f);

		if (CurrentDamage > 0.f)
		{
			float NewHealth = FMath::Clamp(GetHealth() - CurrentDamage, 0.f, GetMaxHealth());
			SetHealth(NewHealth);
		}
	}
	else if (CurrentAttribute == GetPerifyAttribute())
	{
		float CurrentPerify = GetPerify();
		SetPerify(0.f);

		if (CurrentPerify > 0.f)
		{
			float NewPerifyResist = FMath::Clamp(GetPerifyResist() - CurrentPerify, 0.f, GetPerifyMaxResist());
			SetPerifyResist(NewPerifyResist);
		}

	}

}

