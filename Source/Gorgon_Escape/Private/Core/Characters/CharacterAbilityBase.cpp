// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Characters/CharacterAbilityBase.h"
#include "Core/AbilitySystemComponents/CharacterAbilitySystemComponent.h"
#include "Core/AttributeSets/CharacterAttributeSet.h"
#include "GameplayEffect.h"

// Sets default values
ACharacterAbilityBase::ACharacterAbilityBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseAbilitySystemComponent = CreateDefaultSubobject<UCharacterAbilitySystemComponent>(TEXT("BaseAbilitySystemComponent"));
	BaseAttributeSet = CreateDefaultSubobject<UCharacterAttributeSet>(TEXT("BaseAttributeSet"));

}

// Called when the game starts or when spawned
void ACharacterAbilityBase::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACharacterAbilityBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterAbilityBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ACharacterAbilityBase::GetAbilitySystemComponent() const
{
	return BaseAbilitySystemComponent;
}

